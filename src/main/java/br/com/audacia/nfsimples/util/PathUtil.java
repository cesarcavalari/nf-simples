package br.com.audacia.nfsimples.util;

import java.io.File;

public class PathUtil {
  
  private PathUtil() {
    // default
  }
  
  public static String getFilePath(String srcFolderPath, String fileName) {
    return new StringBuilder(srcFolderPath)
        .append(File.separator)
        .append(fileName)
        .toString();
  }
  
  public static String getImportFolderPath(String baseDir, String clientId, String tempFolderUuid) {
    return getDestinationFolder(baseDir, clientId, tempFolderUuid);
  }

  private static String getDestinationFolder(String baseDir, String clientId, String tempFolderUuid) {
    return new StringBuilder(baseDir)
        .append(clientId)
        .append(File.separator)
        .append(tempFolderUuid)
        .toString();
  }
  
  public static String getExportFolderPath(String baseDir, String clientId, String tempFolderUuid) {
    return getDestinationFolder(baseDir, clientId, tempFolderUuid);
  }
  
  public static String getZipFileName(String clientId, String folderId) {
    return new StringBuilder(clientId)
        .append("-")
        .append(folderId)
        .append(ZipUtil.ZIP_EXTESION)
        .toString();
  }
  
  public static String getZipFilePath(String baseDir, String clientId, String folderId) {
    return new StringBuilder(getExportFolderPath(baseDir, clientId, folderId))
        .append(File.separator)
        .append(getZipFileName(clientId, folderId))
        .toString();
  }

}
