package br.com.audacia.nfsimples.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public class NfFileUtil {
  
  public static final String XML_EXTENSION = ".xml";

  private static final XmlMapper xmlMapper = new XmlMapper();

  private NfFileUtil() {
    // default
  }

  public static final JsonNode convertXmlFileToJson(File xmlFile) {

    try {

      byte[] fileByteArray = readFile(xmlFile);

      return xmlMapper.readTree(fileByteArray);

    } catch (IOException e) {
      throw new IllegalStateException("Erro ao converter o arquivo XML para JSON");
    }

  }

  public static byte[] readFile(File file) throws IOException {
    return FileUtils.readFileToByteArray(file);
  }
  
  public static byte[] readFileFromPath(String filePath) {
    try {
      return FileUtils.readFileToByteArray(new File(filePath));
    } catch (IOException e) {
      throw new IllegalStateException("Erro ao ler o arquivo do disco");
    }
  }

  public static final String convertJsonToXml(JsonNode jsonNode) {

    try {
      return xmlMapper.writer().writeValueAsString(jsonNode);
    } catch (IOException e) {
      throw new IllegalStateException("Erro ao converter JSON para XML");
    }

  }
  
  public static final byte[] convertJsonToXmlByteArray(JsonNode jsonNode) {

    try {
      return xmlMapper.writer().writeValueAsBytes(jsonNode);
    } catch (IOException e) {
      throw new IllegalStateException("Erro ao converter JSON para XML");
    }

  }

  public static final void writeXmlToFile(String xml, String filePath) {

    try (FileWriter file = new FileWriter(filePath)) {
      file.write(xml);
      file.flush();
    } catch (IOException e) {
      throw new IllegalStateException("Erro ao escrever conteudo xml no arquivo", e);
    }
    
  }
  
  public static final void writeXmlToFile(byte[] xml, String filePath) {

    try {
      
      Path path = Paths.get(filePath);
      Files.write(path, xml);
    
    } catch (IOException e) {
      throw new IllegalStateException("Erro ao escrever conteudo xml no arquivo", e);
    }
    
  }
  
  public static final void writeNewFile(String destinationDir, String fileName) {
    try {
      writeNewFile(new File(destinationDir), fileName);
    } catch (IOException e) {
      throw new IllegalStateException("Erro ao gravar arquivo no disco", e);
    }
  }
  
  public static final File writeNewFile(File destinationDir, String fileName) throws IOException {
    File destFile = new File(destinationDir, fileName);

    String destDirPath = destinationDir.getCanonicalPath();
    String destFilePath = destFile.getCanonicalPath();

    if (!destFilePath.startsWith(destDirPath + File.separator)) {
      throw new IllegalArgumentException(
          "Entry is outside of the target dir: " + fileName);
    }

    if (destinationDir.exists()) {
      return destFile;
    }

    if (!destinationDir.exists() && destFile.getParentFile().mkdirs()) {
      return destFile;
    }

    throw new IllegalArgumentException("Erro ao criar arquivo no disco");

  }
  
  public static final byte[] readFileFromClassPath(final String path) {
    
    try (InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(path)) {
    
        return IOUtils.toByteArray(in);
    
    } catch (IOException e) {
        throw new IllegalStateException("Erro ao ler arquivo do classpath", e);
    }

}

}
