package br.com.audacia.nfsimples.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import org.springframework.web.multipart.MultipartFile;

public class ZipUtil {
  
  public static final String ZIP_EXTESION = ".zip";

  private ZipUtil() {
    // default
  }

  public static void unzip(MultipartFile file, String destDirPath) {

    try (InputStream is = file.getInputStream(); ZipInputStream zis = new ZipInputStream(is);) {

      ZipEntry zipEntry = zis.getNextEntry();

      while (zipEntry != null) {
        writeFile(zis, zipEntry, destDirPath);

        zipEntry = zis.getNextEntry();
      }

      zis.closeEntry();

    } catch (Exception e) {
      throw new IllegalStateException(e);
    }

  }

  private static void writeFile(ZipInputStream zis, ZipEntry zipEntry, String destDirPath)
      throws IOException {

    File destDir = new File(destDirPath);

    byte[] buffer = new byte[1024];

    File newFile = newFile(destDir, zipEntry);

    try (FileOutputStream fos = new FileOutputStream(newFile.getPath())) {

      int len;

      while ((len = zis.read(buffer)) > 0) {
        fos.write(buffer, 0, len);
      }

    } catch (Exception e) {
      throw new IllegalStateException(e);
    }

  }

  public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
    File destFile = new File(destinationDir, zipEntry.getName());

    String destDirPath = destinationDir.getCanonicalPath();
    String destFilePath = destFile.getCanonicalPath();

    if (!destFilePath.startsWith(destDirPath + File.separator)) {
      throw new IllegalArgumentException(
          "Entry is outside of the target dir: " + zipEntry.getName());
    }

    if (destinationDir.exists()) {
      return destFile;
    }

    if (!destinationDir.exists() && destFile.getParentFile().mkdirs()) {
      return destFile;
    }

    throw new IllegalArgumentException("Error to create the file");

  }

  public static void zipFiles(String srcFolderPath, String zipFileName) {

    try (FileOutputStream fos = new FileOutputStream(PathUtil.getFilePath(srcFolderPath, zipFileName));
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(srcFolderPath),
            path -> path.toString().endsWith(NfFileUtil.XML_EXTENSION));) {

      directoryStream.forEach(xmlFile -> addToZipFile(zipOut, xmlFile.toFile()));

    } catch (IOException e) {
      throw new IllegalStateException("Erro ao compactar todos os arquivos XML", e);
    }

  }

  private static void addToZipFile(ZipOutputStream zipOut, File fileToZip) {
    try (FileInputStream fis = new FileInputStream(fileToZip);) {

      ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
      zipOut.putNextEntry(zipEntry);

      byte[] bytes = new byte[1024];

      int length;

      while ((length = fis.read(bytes)) >= 0) {
        zipOut.write(bytes, 0, length);
      }

    } catch (IOException e) {
      throw new IllegalStateException("Erro adicionar arquivo no arquivo zip");
    }
  }

}
