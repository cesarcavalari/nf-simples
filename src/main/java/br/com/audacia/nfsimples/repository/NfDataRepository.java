package br.com.audacia.nfsimples.repository;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import br.com.audacia.nfsimples.model.NfData;

@Repository
public interface NfDataRepository extends MongoRepository<NfData, String>{

  List<NfData> findByClientIdAndFolderId(String clientId, String folderId);
  
}
