package br.com.audacia.nfsimples.repository;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import br.com.audacia.nfsimples.model.ImportTask;

@Repository
public interface ImportTaskRepository extends MongoRepository<ImportTask, String> {
  
  List<ImportTask> findByIsExecuted(boolean isExecuted);

}
