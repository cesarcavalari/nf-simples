package br.com.audacia.nfsimples.repository;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import br.com.audacia.nfsimples.model.ExportTask;

@Repository
public interface ExportTaskRepository extends MongoRepository<ExportTask, String> {
  
  List<ExportTask> findByIsExecuted(boolean isExecuted);

}
