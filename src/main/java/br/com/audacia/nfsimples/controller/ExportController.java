package br.com.audacia.nfsimples.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import br.com.audacia.nfsimples.model.ExportTask;
import br.com.audacia.nfsimples.model.ExportedFile;
import br.com.audacia.nfsimples.service.ExportService;

@RestController
@RequestMapping("/export")
public class ExportController {
  
  @Autowired
  private ExportService exportService;
  
  @PostMapping
  public ResponseEntity<ExportTask> saveExportTask(@RequestParam String clientId, @RequestParam String folderId) {

    ExportTask exportTask = exportService.save(clientId, folderId);

    return ResponseEntity.status(HttpStatus.CREATED)
        .body(exportTask);

  }

  @GetMapping(value = "/download/{exportTaskId}", produces = {MediaType.APPLICATION_OCTET_STREAM_VALUE})
  public ResponseEntity<byte[]> getExportedZipFile(@PathVariable String exportTaskId) {

    ExportedFile exportedZipFile = exportService.getExportedZipFile(exportTaskId);
    
    return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM)
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + exportedZipFile.getFileName() + "\"")
        .body(exportedZipFile.getFile());
    
  }
  
  @PutMapping("/run")
  public ResponseEntity<ResponseMessage> runExportTasks() {

    exportService.runAllExportTasks();

    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseMessage("Tarefas de exportação rodadas com sucesso."));

  }
  
}
