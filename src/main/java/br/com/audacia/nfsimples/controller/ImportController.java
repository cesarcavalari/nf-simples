package br.com.audacia.nfsimples.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import br.com.audacia.nfsimples.model.ImportTask;
import br.com.audacia.nfsimples.service.ImportService;

@RestController
@RequestMapping("/import")
public class ImportController {

  @Autowired
  private ImportService importService;

  @PostMapping("/upload")
  public ResponseEntity<ImportTask> uploadFile(@RequestParam String clientId, @RequestParam("file") MultipartFile file) {

    ImportTask importZipFile = importService.importZipFile(clientId, file);

    return ResponseEntity.status(HttpStatus.OK)
        .body(importZipFile);

  }
  
  @PutMapping("/run")
  public ResponseEntity<ResponseMessage> runImportTasks() {

    importService.runAllImportTasks();

    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseMessage("Tarefas de importação rodadas com sucesso."));

  }

}
