package br.com.audacia.nfsimples.controller;

public class ResponseMessage {

  private String message;

  public ResponseMessage(String message) {
    this.message = message;
  }

  public ResponseMessage() {
    // default
  }
  
  public String getMessage() {
    return message;
  }

}
