package br.com.audacia.nfsimples.config;

import java.util.HashSet;
import java.util.Set;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import io.swagger.annotations.SwaggerDefinition;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Profile("!test")
public class SwaggerConfig {

  public static final String NFSIMPLES_API_TITLE = "Nota Fiscal Simples API";
  protected static final String NFSIMPLES_API_CONTROLLERS_PACKAGE = "br.com.audacia.nfsimples.controller";
  public static final String NFSIMPLES_API_GROUP_NAME = "accenture-icr-api";

  @Bean(name = "icr-api")
  public Docket apiDocketOnline() {
    return new Docket(DocumentationType.SWAGGER_2)
        .groupName(NFSIMPLES_API_GROUP_NAME)
        .apiInfo(getApiInfo())
        .protocols(getProtocols())
        .select()
        .apis(RequestHandlerSelectors.basePackage(NFSIMPLES_API_CONTROLLERS_PACKAGE))
        .build();
  }

  protected ApiInfo getApiInfo() {
    return new ApiInfoBuilder()
        .title(NFSIMPLES_API_TITLE)
        .description("Nota Fiscal Simples API - Editor de notas fiscais")
        .contact(new Contact("Audacia", "https://www.audacia.com.br", "info@audacia.com.br"))
        .version("1.0.0").build();
  }

  protected Set<String> getProtocols() {
    HashSet<String> protocols = new HashSet<>();
    protocols.add(SwaggerDefinition.Scheme.HTTP.toString());
    return protocols;
  }

}
