package br.com.audacia.nfsimples.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableEmbeddedMongo
@ConditionalOnProperty(value = "app.embedded.mongo.enabled", havingValue = "true", matchIfMissing = false)
public class EmbeddedMongoConfig {

}
