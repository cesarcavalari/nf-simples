//package br.com.audacia.nfsimples.config;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Profile;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//
//@Configuration
//@EnableScheduling
//@Profile("!test")
//public class SchedulerConfig {
//	
//	private static final Logger log = LoggerFactory.getLogger(SchedulerConfig.class);
//	
//	@Scheduled(fixedDelayString = "${app.cache.evict.fixed.delay.in.ms}")
//	public void removeAllNonStaticCaches() {
//		log.info("==> The cache evicts routine has been triggered");
//		apiCacheService.removeAllNonStatic();
//	}
//	
//}


//TODO: criar agendamento para executar tarefas de importacao

//TODO: criar agendamento para executar tarefas de exportacao

//TODO: limpar arquivos importados e exportados com uma certa frequencia

//TODO: limpar base de dados: se existir tarefa de importacao com tempo maior que o tempo de execucao de tarefas de importacao, remover.

//TODO: limpar base de dados: se houver tarefas de exportacao com tempo maior que o tempo de execucao de tarefas de exportacao, remover.