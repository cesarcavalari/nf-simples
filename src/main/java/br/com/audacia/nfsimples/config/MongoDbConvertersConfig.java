package br.com.audacia.nfsimples.config;

import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import br.com.audacia.nfsimples.converter.DocumentToJsonNodeConverter;
import br.com.audacia.nfsimples.converter.JsonNodeToDocumentConverter;

@Configuration
public class MongoDbConvertersConfig {
  
  @Bean
  public MongoCustomConversions mongoCustomConversions() {
      List<Converter<?, ?>> converters = new ArrayList<>();
      converters.add(JsonNodeToDocumentConverter.INSTANCE);
      converters.add(DocumentToJsonNodeConverter.INSTANCE);
      return new MongoCustomConversions(converters);
  }

}
