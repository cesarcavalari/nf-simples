package br.com.audacia.nfsimples.model;

import java.time.LocalDateTime;

public abstract class Task {

  private String id;
  private String clientId;
  private String folderId;
  private LocalDateTime creationDate;
  private LocalDateTime executionDate;
  private boolean isExecuted;
  
  public Task() {
    // default
  }

  public Task(String clientId, String folderId, LocalDateTime creationDate, boolean isExecuted) {
    this.clientId = clientId;
    this.folderId = folderId;
    this.creationDate = creationDate;
    this.isExecuted = isExecuted;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getFolderId() {
    return folderId;
  }

  public void setFolderId(String folderId) {
    this.folderId = folderId;
  }

  public LocalDateTime getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(LocalDateTime creationDate) {
    this.creationDate = creationDate;
  }

  public boolean isExecuted() {
    return isExecuted;
  }

  public void setExecuted(boolean isExecuted) {
    this.isExecuted = isExecuted;
  }

  public LocalDateTime getExecutionDate() {
    return executionDate;
  }

  public void setExecutionDate(LocalDateTime executionDate) {
    this.executionDate = executionDate;
  }

}
