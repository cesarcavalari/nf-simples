package br.com.audacia.nfsimples.model;

import java.time.LocalDateTime;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ImportTask extends Task{
  
  public ImportTask() {
    super();
  }

  public ImportTask(String clientId, String folderId, LocalDateTime creationDate, boolean isExecuted) {
    super(clientId, folderId, creationDate, isExecuted);
  }

}
