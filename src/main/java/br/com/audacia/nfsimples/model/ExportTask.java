package br.com.audacia.nfsimples.model;

import java.time.LocalDateTime;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ExportTask extends Task{
  
  private String zipFilePath;
  private String zipFileName;
  
  public ExportTask() {
    super();
  }

  public ExportTask(String clientId, String folderId, LocalDateTime creationDate, boolean isExecuted) {
    super(clientId, folderId, creationDate, isExecuted);
  }

  public String getZipFilePath() {
    return zipFilePath;
  }

  public void setZipFilePath(String zipFilePath) {
    this.zipFilePath = zipFilePath;
  }

  public String getZipFileName() {
    return zipFileName;
  }

  public void setZipFileName(String zipFileName) {
    this.zipFileName = zipFileName;
  }

}
