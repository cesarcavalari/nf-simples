package br.com.audacia.nfsimples.model;

public class ExportedFile {

  private String fileName;
  private byte[] file;

  public ExportedFile(String fileName, byte[] file) {
    this.fileName = fileName;
    this.file = file;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public byte[] getFile() {
    return file;
  }

  public void setFile(byte[] file) {
    this.file = file;
  }

}
