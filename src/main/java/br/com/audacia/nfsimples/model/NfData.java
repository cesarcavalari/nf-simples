package br.com.audacia.nfsimples.model;

import java.time.LocalDateTime;
import com.fasterxml.jackson.databind.JsonNode;

public class NfData {

  private String id;
  private String clientId;
  private String folderId;
  private LocalDateTime importDate;
  private JsonNode notaFiscal;
  private String originalFileName;
  
  public NfData() {
    // default
  }
  
  public NfData(String id, String clientId, String folderId, LocalDateTime importDate, JsonNode notaFiscal, String originalFileName) {
    this.id = id;
    this.clientId = clientId;
    this.folderId = folderId;
    this.importDate = importDate;
    this.notaFiscal = notaFiscal;
    this.originalFileName = originalFileName;
  }

  public NfData(String clientId, String folderId, LocalDateTime importDate, JsonNode notaFiscal, String originalFileName) {
    this.clientId = clientId;
    this.folderId = folderId;
    this.importDate = importDate;
    this.notaFiscal = notaFiscal;
    this.originalFileName = originalFileName;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public LocalDateTime getImportDate() {
    return importDate;
  }

  public void setImportDate(LocalDateTime importDate) {
    this.importDate = importDate;
  }

  public JsonNode getNotaFiscal() {
    return notaFiscal;
  }

  public void setNotaFiscal(JsonNode notaFiscal) {
    this.notaFiscal = notaFiscal;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getOriginalFileName() {
    return originalFileName;
  }

  public void setOriginalFileName(String originalFileName) {
    this.originalFileName = originalFileName;
  }

  public String getFolderId() {
    return folderId;
  }

  public void setFolderId(String folderId) {
    this.folderId = folderId;
  }

}
