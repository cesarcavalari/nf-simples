package br.com.audacia.nfsimples.converter;

import java.io.IOException;
import org.bson.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@ReadingConverter
public enum DocumentToJsonNodeConverter implements Converter<Document, JsonNode> {
    INSTANCE;

    public JsonNode convert(Document source) {
        if(source == null)
            return null;

        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readTree(source.toJson());
        } catch (IOException e) {
            throw new IllegalStateException("Unable to parse DbObject to JsonNode", e);
        }
    }
}
