package br.com.audacia.nfsimples.converter;

import org.bson.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;
import com.fasterxml.jackson.databind.JsonNode;

@WritingConverter
public enum JsonNodeToDocumentConverter implements Converter<JsonNode, Document> {
    INSTANCE;

    public Document convert(JsonNode source) {
        if(source == null)
            return null;

        return Document.parse(source.toString());
    }

}