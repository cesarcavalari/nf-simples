package br.com.audacia.nfsimples.service;

import static br.com.audacia.nfsimples.util.PathUtil.getExportFolderPath;
import static br.com.audacia.nfsimples.util.PathUtil.getZipFileName;
import static br.com.audacia.nfsimples.util.PathUtil.getZipFilePath;
import java.io.File;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import br.com.audacia.nfsimples.model.ExportTask;
import br.com.audacia.nfsimples.model.ExportedFile;
import br.com.audacia.nfsimples.model.NfData;
import br.com.audacia.nfsimples.repository.ExportTaskRepository;
import br.com.audacia.nfsimples.repository.NfDataRepository;
import br.com.audacia.nfsimples.util.NfFileUtil;
import br.com.audacia.nfsimples.util.ZipUtil;

@Service
public class ExportService {

  @Value("${app.temp.export.dir}")
  private String tempExportDir;

  private NfDataRepository nfDataRepository;
  private ExportTaskRepository exportTaskRepository;

  public ExportService(ExportTaskRepository exportTaskRepository,
      NfDataRepository nfDataRepository) {
    this.exportTaskRepository = exportTaskRepository;
    this.nfDataRepository = nfDataRepository;
  }

  public ExportTask save(String clientId, String folderId) {

    if(StringUtils.isEmpty(clientId) || StringUtils.isEmpty(folderId)) {
      throw new IllegalArgumentException("O id do cliente e o id da pasta são obrigatórios.");
    }
    
    ExportTask exportTask = new ExportTask(clientId, folderId, LocalDateTime.now(), false);
    exportTask.setZipFilePath(getZipFilePath(tempExportDir, exportTask.getClientId(), exportTask.getFolderId()));
    exportTask.setZipFileName(getZipFileName(exportTask.getClientId(), exportTask.getFolderId()));
    return exportTaskRepository.save(exportTask);
    
  }
  
  public void runAllExportTasks() {
    List<ExportTask> pendingExportTasks = exportTaskRepository.findByIsExecuted(false);
    
    pendingExportTasks.stream().forEach(this :: runExportTask);
  }

  /**
   * 
   * Executa uma tarefa de exportação.
   * Deve ser chamada de forma assíncrona.
   * 
   * @param exportTask
   */
  public void runExportTask(ExportTask exportTask) {

    String clientId = exportTask.getClientId();
    String folderId = exportTask.getFolderId();

    List<NfData> nfDatas = nfDataRepository.findByClientIdAndFolderId(clientId, folderId);
    
    if(nfDatas == null || nfDatas.isEmpty()) {
      throw new IllegalStateException("Não foram encontrados dados de notas fiscais para o cliente informado.");
    }
    
    nfDatas.stream().forEach(nfData -> {
      byte[] xmlResult = NfFileUtil.convertJsonToXmlByteArray(nfData.getNotaFiscal());
      NfFileUtil.writeNewFile(getExportFolderPath(tempExportDir, clientId, folderId), nfData.getOriginalFileName());
      NfFileUtil.writeXmlToFile(xmlResult, getExportedNfFileName(tempExportDir, clientId, folderId, nfData.getOriginalFileName()));
    });

//    nfDatas.stream().forEach(nfData -> {
//      String xmlResult = NfFileUtil.convertJsonToXml(nfData.getNotaFiscal());
//      NfFileUtil.writeNewFile(getExportFolderPath(tempExportDir, clientId, folderId), nfData.getOriginalFileName());
//      NfFileUtil.writeXmlToFile(xmlResult, getExportedNfFileName(tempExportDir, clientId, folderId, nfData.getOriginalFileName()));
//    });
    
    ZipUtil.zipFiles(getExportFolderPath(tempExportDir, clientId, folderId), getZipFileName(clientId, folderId));

    exportTask.setExecuted(true);
    exportTask.setExecutionDate(LocalDateTime.now());

    exportTaskRepository.save(exportTask);
  }

  private String getExportedNfFileName(String baseDir, String clientId, String folderId, String originalFileName) {
    return new StringBuilder(getExportFolderPath(baseDir, clientId, folderId))
        .append(File.separator)
        .append(originalFileName)
        .toString();
  }
  
  public ExportedFile getExportedZipFile(String exportTaskId) {
    Optional<ExportTask> exportTask = exportTaskRepository.findById(exportTaskId);
    
    if(!exportTask.isPresent()) {
      throw new IllegalStateException("Tarefa de exportação não encontrada.");
    }
    
    byte[] zipFile = NfFileUtil.readFileFromPath(exportTask.get().getZipFilePath());
    
    return new ExportedFile(exportTask.get().getZipFileName(), zipFile);
  }

}
