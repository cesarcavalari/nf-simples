package br.com.audacia.nfsimples.service;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import com.fasterxml.jackson.databind.JsonNode;
import br.com.audacia.nfsimples.model.ImportTask;
import br.com.audacia.nfsimples.model.NfData;
import br.com.audacia.nfsimples.repository.ImportTaskRepository;
import br.com.audacia.nfsimples.repository.NfDataRepository;
import br.com.audacia.nfsimples.util.NfFileUtil;
import br.com.audacia.nfsimples.util.PathUtil;
import br.com.audacia.nfsimples.util.ZipUtil;

@Service
public class ImportService {
  
  @Value("${app.temp.unzip.dir}")
  private String tempUnzipDir;

  private NfDataRepository nfDataRepository;
  private ImportTaskRepository importTaskRepository;

  public ImportService(NfDataRepository nfDataRepository, ImportTaskRepository importTaskRepository) {
    this.nfDataRepository = nfDataRepository;
    this.importTaskRepository = importTaskRepository;
  }

  public ImportTask importZipFile(String clientId, MultipartFile file) {

    if (StringUtils.isEmpty(clientId)) {
      throw new IllegalArgumentException("O cliente não foi informado");
    }

    String tempFolderUuid = UUID.randomUUID().toString();

    ZipUtil.unzip(file, PathUtil.getImportFolderPath(tempUnzipDir, clientId, tempFolderUuid));

    return saveImportTask(new ImportTask(clientId, tempFolderUuid, LocalDateTime.now(), false));

  }

  private void saveNfDatas(ImportTask importTask, Map<String, JsonNode> nfsAsJson) {
    List<NfData> nfDatas = new ArrayList<>();

    nfsAsJson.entrySet().stream().forEach(entry -> {
      JsonNode nfAsJson = entry.getValue();
      String originalFileName = entry.getKey();
      NfData nfData = new NfData(importTask.getClientId(), importTask.getFolderId(), LocalDateTime.now(), nfAsJson, originalFileName);
      nfDatas.add(nfData);
    });

    nfDataRepository.saveAll(nfDatas);
  }

  private ImportTask saveImportTask(ImportTask importTask) {
    return importTaskRepository.save(importTask);
  }

  private void setImportTaskAsExecuted(ImportTask importTask) {
    importTask.setExecuted(true);
    importTask.setExecutionDate(LocalDateTime.now());
    importTaskRepository.save(importTask);
  }

  public void runAllImportTasks() {
    List<ImportTask> pendingImportTasks = importTaskRepository.findByIsExecuted(false);
    pendingImportTasks.stream().forEach(this :: runImportXmlToJsonTask);
  }
  
  public void runImportXmlToJsonTask(ImportTask importTask) {

    String srcFolderPath = PathUtil.getImportFolderPath(tempUnzipDir, importTask.getClientId(), importTask.getFolderId());

    Map<String, JsonNode> nfInJson = new HashMap<>();

    try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(srcFolderPath),
        path -> path.toString().endsWith(NfFileUtil.XML_EXTENSION));) {

      directoryStream.forEach(xmlFile -> {
        JsonNode jsonContent = NfFileUtil.convertXmlFileToJson(xmlFile.toFile());
        nfInJson.put(xmlFile.getFileName().toString(), jsonContent);
      });

    } catch (IOException e) {
      throw new IllegalStateException("Erro ao converter o arquivo XML para JSON", e);
    }

    saveNfDatas(importTask, nfInJson);

    setImportTaskAsExecuted(importTask);

  }
}
