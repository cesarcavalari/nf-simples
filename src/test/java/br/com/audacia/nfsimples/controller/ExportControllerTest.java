package br.com.audacia.nfsimples.controller;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import br.com.audacia.nfsimples.model.ExportTask;
import br.com.audacia.nfsimples.model.ImportTask;
import br.com.audacia.nfsimples.util.NfFileUtil;

@SpringBootTest
@AutoConfigureMockMvc
public class ExportControllerTest {

  @Autowired
  private MockMvc mockMvc;
  
  private static ObjectMapper objectMapper;
  
  @BeforeAll
  public static void setUp() {
    objectMapper = new ObjectMapper();
    objectMapper.registerModule(new JavaTimeModule());
  }
  
  @Test
  public void whenSaveExportTask_thenVerifyStatusCreate() throws Exception {
    
    byte[] zipFile = NfFileUtil.readFileFromClassPath("static/nf-example.zip");

    MockMultipartFile file = new MockMultipartFile("nf-example", "nf-example.zip",
        MediaType.APPLICATION_OCTET_STREAM_VALUE, zipFile);

    MvcResult result = this.mockMvc
        .perform(multipart("/import/upload?clientId=1").file("file", file.getBytes())
            .characterEncoding("UTF-8"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").isNotEmpty())
        .andExpect(jsonPath("$.clientId").isNotEmpty())
        .andExpect(jsonPath("$.folderId").isNotEmpty())
        .andReturn();
    
    String contentAsString = result.getResponse().getContentAsString();
    
    ImportTask importTask = objectMapper.readValue(contentAsString, ImportTask.class);
    
    this.mockMvc
      .perform(put("/import/run"))
      .andDo(print())
      .andExpect(status().isOk());

    this.mockMvc
      .perform(post("/export").queryParam("clientId", importTask.getClientId()).queryParam("folderId", importTask.getFolderId()))
      .andDo(print())
      .andExpect(status().isCreated())
      .andExpect(jsonPath("$.id").isNotEmpty())
      .andExpect(jsonPath("$.clientId").isNotEmpty())
      .andExpect(jsonPath("$.folderId").isNotEmpty());

  }
  
  @Test
  public void whenDownloadExportedFile_thenVerifyStatusOK() throws Exception {
    
    byte[] zipFile = NfFileUtil.readFileFromClassPath("static/nf-example.zip");

    MockMultipartFile file = new MockMultipartFile("nf-example", "nf-example.zip",
        MediaType.APPLICATION_OCTET_STREAM_VALUE, zipFile);

    MvcResult result = this.mockMvc
        .perform(multipart("/import/upload?clientId=1").file("file", file.getBytes())
            .characterEncoding("UTF-8"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").isNotEmpty())
        .andExpect(jsonPath("$.clientId").isNotEmpty())
        .andExpect(jsonPath("$.folderId").isNotEmpty())
        .andReturn();
    
    String contentAsString = result.getResponse().getContentAsString();
    
    ImportTask importTask = objectMapper.readValue(contentAsString, ImportTask.class);

    MvcResult exportResult = this.mockMvc
      .perform(post("/export").queryParam("clientId", importTask.getClientId()).queryParam("folderId", importTask.getFolderId()))
      .andDo(print())
      .andExpect(status().isCreated())
      .andExpect(jsonPath("$.id").isNotEmpty())
      .andExpect(jsonPath("$.clientId").isNotEmpty())
      .andExpect(jsonPath("$.folderId").isNotEmpty())
      .andReturn();
    
    String exportContent = exportResult.getResponse().getContentAsString();
    
    ExportTask exportTask = objectMapper.readValue(exportContent, ExportTask.class);
    
    this.mockMvc
      .perform(put("/export/run"))
      .andDo(print())
      .andExpect(status().isOk());

    MvcResult downloadResult = this.mockMvc
      .perform(get("/export/download/" + exportTask.getId()))
      .andDo(print())
      .andExpect(status().isOk())
      .andReturn();
    
    byte[] contentAsByteArray = downloadResult.getResponse().getContentAsByteArray();
    
    assertNotNull(contentAsByteArray);

  }

}
