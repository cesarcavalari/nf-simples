package br.com.audacia.nfsimples.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import br.com.audacia.nfsimples.util.NfFileUtil;

@SpringBootTest
@AutoConfigureMockMvc
public class ImportControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void whenFileUploaded_thenVerifyStatus() throws Exception {
    byte[] zipFile = NfFileUtil.readFileFromClassPath("static/nf-example.zip");

    MockMultipartFile file = new MockMultipartFile("nf-example", "nf-example.zip",
        MediaType.APPLICATION_OCTET_STREAM_VALUE, zipFile);

    this.mockMvc
        .perform(multipart("/import/upload?clientId=1").file("file", file.getBytes())
            .characterEncoding("UTF-8"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").isNotEmpty())
        .andExpect(jsonPath("$.clientId").isNotEmpty())
        .andExpect(jsonPath("$.folderId").isNotEmpty());

  }

  @Test
  public void whenRunImportTask_thenVerifyStatusOK() throws Exception {

    this.mockMvc
      .perform(put("/import/run"))
      .andDo(print())
      .andExpect(status().isOk());

  }

}
